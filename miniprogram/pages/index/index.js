Page({

  data: {
    
  },

  onLoad: function (options) {
    
  },

  onSwitchCheck(e) {
    wx.showToast({title: e.detail.title, icon: 'none'})
  },

  onToChildrenData(e) {
    this.setData({
      checked: e.detail.value ? true : false
    })
  },

})