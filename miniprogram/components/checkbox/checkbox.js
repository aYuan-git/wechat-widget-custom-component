Component({
  properties: {
    checked: {
      type: Boolean
    }
  },
  data: {
    age: 24
  },
  methods: {
    // 向父组件传数据用this.triggerEvent
    checkActive() {
      this.triggerEvent('switchCheck', {title: '我是打工人'})
    }
  },
  lifetimes: {
    attached() {
      console.log('1', this.data.age)
    }
  },
  pageLifetimes: {
    show() {
      console.log('2', this.data.age)
    }
  }
})